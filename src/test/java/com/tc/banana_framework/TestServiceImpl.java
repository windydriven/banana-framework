package com.tc.banana_framework;

import com.tc.banana.framework.annotation.Service;

/** 
 * @author  wdd 
 * @date 创建时间：2016年4月3日 下午2:51:41 
 */
@Service("nihao")
public class TestServiceImpl implements TestService {

	public void add() {
		System.out.println("=================add");
	}

}
