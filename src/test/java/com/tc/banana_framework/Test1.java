package com.tc.banana_framework;

import java.lang.reflect.Method;

import org.junit.Test;

import com.tc.banana.framework.BeanHelper;
import com.tc.banana.framework.HelperLoader;
import com.tc.banana.framework.util.ReflectionUtil;

/** 
 * @author  wdd 
 * @date 创建时间：2016年4月3日 下午2:33:36 
 */
public class Test1 {

	@Test
	public void testObjName(){
		/*ReflectionUtil obj = new ReflectionUtil();
		String className = obj.getClass().getSimpleName();
		System.out.println(className);*/
		boolean b = TestService.class.isAssignableFrom(TestServiceImpl.class);
		System.out.println(b);
	}
	
	@Test
	public void testController (){
		HelperLoader.init();
		TestController bean = BeanHelper.getBean(TestController.class);
		bean.testAdd();
	}
	
	@Test
	public void testReflect () {
		TestMethod methodObj = new TestMethod();
		for (Method method : methodObj.getClass().getMethods()) {
			if (method.getName().equals("say") ){
				ReflectionUtil.invokeMethod(methodObj, method, "asd");
			}
		}
	}
	
	
	@Test
	public void testCCC(){
		System.out.println("asdas".lastIndexOf("?"));
	}
}
