package com.tc.banana_framework;

import com.tc.banana.framework.annotation.Controller;
import com.tc.banana.framework.annotation.Resource;

/** 
 * @author  wdd 
 * @date 创建时间：2016年4月3日 下午2:50:03 
 */
@Controller
public class TestController {
	
	@Resource
	private TestService testService;
	
	public void testAdd(){
		testService.add();
	}
	
	
}
