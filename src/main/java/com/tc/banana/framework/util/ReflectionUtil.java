package com.tc.banana.framework.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
 * @author  wdd 
 * @date 创建时间：2016年4月3日 下午2:01:56 
 */
public class ReflectionUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReflectionUtil.class);
	
	public static Object newInstance(Class<?> cls) {
		Object instance = null;
		try {
			instance = cls.newInstance();
			LOGGER.info(cls + " is instanced");
		} catch (InstantiationException e) {
			LOGGER.error("instance class failure", e);
		} catch (IllegalAccessException e) {
			LOGGER.error("instance class failure", e);
		}
		return instance;
	}
	
	public static Object invokeMethod(Object obj, Method method, Object ...args) {
		Object result = null;
		
		method.setAccessible(true);
		try {
			result = method.invoke(obj, args);
		} catch (Exception e) {
			LOGGER.error("invork method failure", e);
		} 
		
		return result;
	}

	public static void setField(Object obj, Field field, Object value) {
		field.setAccessible(true);
		try {
			field.set(obj, value);
			LOGGER.info("set field:"+ field.getName());
		} catch (IllegalArgumentException e) {
			LOGGER.error("set field failure", e);
		} catch (IllegalAccessException e) {
			LOGGER.error("set field failure", e);
		}
	}
	
}
