package com.tc.banana.framework.util;

import org.apache.commons.lang3.StringUtils;

/** 
 * @author  wdd 
 * @date 创建时间：2016年4月3日 下午9:30:20 
 */
public class StringUtil {
	
	/**
	 * 数组转成url
	 * @author: wangdongdong
	 * @date: 2016年4月6日       
	 * @param value
	 * @return
	 */
	public static String[] toUri(String [] value){
		String [] uris = new String[value.length];
		for (int i = 0; i<value.length; i++) {
			uris[i] = value[i].startsWith("/") ? value[i] : "/" + value[i];
		}
		return uris;
	}
	
	/**
	 * 字符串数组是否包含某字符串
	 * @author: wangdongdong
	 * @date: 2016年4月6日       
	 * @param value
	 * @param values
	 * @return
	 */
	public static boolean isContainValue(String value, String [] values) {
		for (String str : values) {
			if (str.equals(value)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isBlank (String value) {
		return StringUtils.isBlank(value);
	}
	
	public static boolean isNotBlank (String value) {
		return !isBlank(value);
	}
	
	public static boolean isEmpty (String value) {
		return StringUtils.isEmpty(value);
	}
	
	public static boolean isNotEmpty (String value) {
		return !isEmpty(value);
	}
}
