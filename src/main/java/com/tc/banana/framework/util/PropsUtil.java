package com.tc.banana.framework.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @Description:TODO 属性文件工具类  
 * @author: wangdongdong  
 * @date:   2016年3月31日 上午9:59:03   
 *
 */
public class PropsUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(PropsUtil.class);
	
	/**
	 * 加载属性文件
	 * @author: wangdongdong
	 * @date: 2016年3月31日       
	 * @param propFile
	 * @return
	 */
	public static Properties loadProps(String propFile){
		Properties properties = null;
		InputStream is = null;
		try {
			is = Thread.currentThread().getContextClassLoader().getResourceAsStream(propFile);
			if (is == null) {
				throw new FileNotFoundException(propFile + " file is not found");
			}
			properties = new Properties();
			properties.load(is);
			logger.info("------load properties from file " + propFile +"-------");
		} catch (IOException e) {
			logger.error("----load properties file failure----", e);
		} finally {
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					logger.error("----close inputStream failure----", e);
				}
			}
		}
		return properties;
	}
	
	/**
	 * 获取字符型属性 (默认为空)
	 * @author: wangdongdong
	 * @date: 2016年3月31日       
	 * @param properties
	 * @param property
	 * @return
	 */
	public static String getString(final Properties properties, String property){
		return getString(properties, property, "");
	}
	
	/**
	 * 获取字符属性 (可指定默认值)
	 * @author: wangdongdong
	 * @date: 2016年3月31日       
	 * @param properties
	 * @param property
	 * @param defaultValue
	 * @return
	 */
	public static String getString(final Properties properties, String property, String defaultValue){
		return properties.getProperty(property, defaultValue);
	}
	
	/**
	 * 获取数值型属性 (默认值为0)
	 * @author: wangdongdong
	 * @date: 2016年3月31日       
	 * @param properties
	 * @param property
	 * @return
	 */
	public static int getInt(final Properties properties, String property){
		return getInt(properties, property, 0);
	}

	/**
	 * 获取数值型属性 (可指定默认值)
	 * @author: wangdongdong
	 * @date: 2016年3月31日       
	 * @param properties
	 * @param property
	 * @param defaultValue
	 * @return
	 */
	public static int getInt(final Properties properties, String property, int defaultValue){
		int intValue = defaultValue;
		if(properties.containsKey(property)){
			intValue = CastUtil.castInt(properties.get(property));
		}
		return intValue;
	}
	
}
