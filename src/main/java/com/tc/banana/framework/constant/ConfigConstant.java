package com.tc.banana.framework.constant;

public interface ConfigConstant {
	
	String CONFIG_FILE = "banana.properties";
	
	String JDBC_DRIVER = "banana.framework.jdbc.driver";
	
	String JDBC_URL = "banana.framework.jdbc.url";
	
	String JDBC_USERNAME = "banana.framework.jdbc.username";
	
	String JDBC_PASSWORD = "banana.framework.jdbc.password";
	
	String APP_BASE_PACKAGE = "banana.framework.app.base_pakage";
	
	String APP_JSP_PATH = "banana.framework.app.jsp_path";
	
	String APP_ASSET_PATH = "banana.framework.app.asset_path";
			
}
