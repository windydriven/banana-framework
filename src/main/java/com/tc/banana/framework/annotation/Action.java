package com.tc.banana.framework.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.tc.banana.framework.enums.RequestMethod;

/** 
 * @author  wdd 
 * @date 创建时间：2016年4月2日 上午11:23:43 
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Action {
	/** 请求路径 */
	String [] value() default {};
	/** 请求方式 */
	RequestMethod  method() default RequestMethod.GET;
}
