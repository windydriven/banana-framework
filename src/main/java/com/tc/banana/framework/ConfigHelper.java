package com.tc.banana.framework;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tc.banana.framework.constant.ConfigConstant;
import com.tc.banana.framework.util.PropsUtil;

public final class ConfigHelper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigHelper.class);
	
	private static final Properties CONFIG_PROPS = PropsUtil.loadProps(ConfigConstant.CONFIG_FILE);
	
	public static String getJdbcDriver(){
		LOGGER.info("get property value of " +ConfigConstant.JDBC_DRIVER);
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_DRIVER);
	}
	
	public static String getJdbcUrl(){
		LOGGER.info("get property value of " +ConfigConstant.JDBC_URL);
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_URL);
	}
	
	public static String getJdbcUserName(){
		LOGGER.info("get property value of " +ConfigConstant.JDBC_USERNAME);
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_USERNAME);
	}
	
	public static String getJdbcPassword(){
		LOGGER.info("get property value of " +ConfigConstant.JDBC_PASSWORD);
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.JDBC_PASSWORD);
	}
	
	public static String getAppBasePackage(){
		LOGGER.info("get property value of " +ConfigConstant.APP_BASE_PACKAGE);
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.APP_BASE_PACKAGE);
	}
	
	public static String getAppJspPath(){
		LOGGER.info("get property value of " +ConfigConstant.APP_JSP_PATH);
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.APP_JSP_PATH, "/WEB-INF/view/");
	}
	
	public static String getAppAssetPath(){
		LOGGER.info("get property value of " +ConfigConstant.APP_ASSET_PATH);
		return PropsUtil.getString(CONFIG_PROPS, ConfigConstant.APP_ASSET_PATH, "/asset/");
	}
	
}
