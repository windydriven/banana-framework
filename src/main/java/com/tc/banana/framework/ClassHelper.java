package com.tc.banana.framework;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Set;

import com.tc.banana.framework.annotation.Controller;
import com.tc.banana.framework.annotation.Service;
import com.tc.banana.framework.util.ClassUtil;

/**
 * 类加载助手类
 * 
 * @author wdd
 * @date 创建时间：2016年4月2日 上午11:31:25
 */
public class ClassHelper {

	private static final Set<Class<?>> CLASS_SET;

	static {
		String basePackageName = ConfigHelper.getAppBasePackage();
		CLASS_SET = ClassUtil.getClassSet(basePackageName);
	}

	public static Set<Class<?>> getClassSet() {
		return CLASS_SET;
	}

	public static Set<Class<?>> getAnnotationClassSet(
			Class<? extends Annotation> annotationClass) {
		Set<Class<?>> classSet = new HashSet<Class<?>>();
		   for (Class<?> cls : CLASS_SET) {
			if (cls.isAnnotationPresent(annotationClass)) {
				classSet.add(cls);
			}
		}
		return classSet;
	}

	public static Set<Class<?>> getBeanClassSet() {
		Set<Class<?>> classSet = new HashSet<Class<?>>();
		classSet.addAll(getAnnotationClassSet(Controller.class));
		classSet.addAll(getAnnotationClassSet(Service.class));
		return classSet;
	}
}
