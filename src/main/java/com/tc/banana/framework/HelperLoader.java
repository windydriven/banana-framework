package com.tc.banana.framework;

import com.tc.banana.framework.util.ClassUtil;

/** 
 * @author  wdd 
 * @date 创建时间：2016年4月3日 下午2:56:06 
 */
public final class HelperLoader {
	public static void init(){
		Class<?> [] classList = {IocHelper.class, ClassHelper.class, BeanHelper.class, ServletHelper.class};
		for (Class<?> cls : classList){
			ClassUtil.loadClass(cls.getName());
		}
		IocHelper.init();
	}
}
