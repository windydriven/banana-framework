package com.tc.banana.framework.enums;
/** 
 * @author  wdd 
 * @date 创建时间：2016年4月2日 上午11:28:03 
 */
public enum RequestMethod {
	
	POST("POST"),
	
	GET("GET");
	
	RequestMethod (String name){
		this.name = name;
	}
	
	private final String name;

	public String getName() {
		return name;
	}
	
}
