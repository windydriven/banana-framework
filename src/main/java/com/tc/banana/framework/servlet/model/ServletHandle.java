package com.tc.banana.framework.servlet.model;

import java.lang.reflect.Method;

import com.tc.banana.framework.enums.RequestMethod;

/** 
 * @author  wdd 
 * @date 创建时间：2016年4月3日 下午9:08:32 
 */
public class ServletHandle {
	/** 请求地址 */
	private String [] reqestUri;
	/** 方法 */
	private RequestMethod requestMethod;
	/** 对应的方法 */
	private Method method;
	/** 对应的对象 */
	private Object object;
	
	public ServletHandle(String[] reqestUri, RequestMethod requestMethod, Method method, Object object) {
		super();
		this.reqestUri = reqestUri;
		this.requestMethod = requestMethod;
		this.method = method;
		this.object = object;
	}
	
	public String[] getReqestUri() {
		return reqestUri;
	}
	public void setReqestUri(String[] reqestUri) {
		this.reqestUri = reqestUri;
	}
	public RequestMethod getRequestMethod() {
		return requestMethod;
	}
	public void setRequestMethod(RequestMethod requestMethod) {
		this.requestMethod = requestMethod;
	}
	public Method getMethod() {
		return method;
	}
	public void setMethod(Method method) {
		this.method = method;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	
}
