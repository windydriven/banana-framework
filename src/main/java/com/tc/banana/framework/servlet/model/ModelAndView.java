package com.tc.banana.framework.servlet.model;

public class ModelAndView {
	
	private Param param;
	
	private String viewName;
	
	public ModelAndView(String viewName) {
		this.viewName = viewName;
	}
	
	public ModelAndView(){}
	
	public ModelAndView (String viewName, Param param) {
		this.viewName = viewName;
		this.param = param;
	}
	
	public void addObject (String name, Object value) {
		getParam().put(name, value);
	}

	public Param getParam() {
		if (param == null) {
			return new Param();
		}
		return param;
	}

	public String getViewName() {
		return viewName;
	}

}
