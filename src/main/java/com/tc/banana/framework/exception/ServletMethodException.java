package com.tc.banana.framework.exception;

import javax.servlet.ServletException;

public class ServletMethodException extends ServletException {

	/**   
	 * @Fields serialVersionUID :
	 */ 
	private static final long serialVersionUID = 1L;
	
	public ServletMethodException() {
		super("Servlet请求方法异常");
	}
	
	public ServletMethodException(String errorMsg) {
		super(errorMsg);
	}
	
	public ServletMethodException(String errorMsg, Throwable cause) {
		super(errorMsg, cause);
	}
	
}
